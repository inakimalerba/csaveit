/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionDescargar_Datos;
    QAction *actionActon;
    QWidget *centralWidget;
    QPushButton *button_enviar;
    QTableWidget *tableWidget;
    QPushButton *button_limpiar;
    QLineEdit *TEnombre;
    QLineEdit *TElegajo;
    QSplitter *splitter;
    QLabel *label;
    QLabel *label_3;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_8;
    QPushButton *button_eliminar;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QLineEdit *TEemail;
    QLineEdit *TEcelular;
    QFrame *frame;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout_2;
    QRadioButton *primeranio;
    QRadioButton *segundoanio;
    QRadioButton *terceranio;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_2;
    QLineEdit *TEapellido;
    QLabel *label_4;
    QLabel *label_7;
    QLineEdit *TEfacebook;
    QComboBox *CBcarrera;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(585, 601);
        MainWindow->setMinimumSize(QSize(585, 601));
        MainWindow->setMaximumSize(QSize(585, 601));
        actionDescargar_Datos = new QAction(MainWindow);
        actionDescargar_Datos->setObjectName(QStringLiteral("actionDescargar_Datos"));
        actionActon = new QAction(MainWindow);
        actionActon->setObjectName(QStringLiteral("actionActon"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        button_enviar = new QPushButton(centralWidget);
        button_enviar->setObjectName(QStringLiteral("button_enviar"));
        button_enviar->setGeometry(QRect(410, 302, 81, 22));
        button_enviar->setAutoDefault(true);
        tableWidget = new QTableWidget(centralWidget);
        if (tableWidget->columnCount() < 11)
            tableWidget->setColumnCount(11);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(9, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(10, __qtablewidgetitem10);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(30, 328, 521, 199));
        button_limpiar = new QPushButton(centralWidget);
        button_limpiar->setObjectName(QStringLiteral("button_limpiar"));
        button_limpiar->setGeometry(QRect(120, 302, 81, 22));
        TEnombre = new QLineEdit(centralWidget);
        TEnombre->setObjectName(QStringLiteral("TEnombre"));
        TEnombre->setGeometry(QRect(101, 111, 180, 31));
        TEnombre->setAutoFillBackground(true);
        TElegajo = new QLineEdit(centralWidget);
        TElegajo->setObjectName(QStringLiteral("TElegajo"));
        TElegajo->setGeometry(QRect(101, 150, 180, 31));
        TElegajo->setAutoFillBackground(true);
        splitter = new QSplitter(centralWidget);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setGeometry(QRect(30, 111, 71, 191));
        splitter->setOrientation(Qt::Vertical);
        label = new QLabel(splitter);
        label->setObjectName(QStringLiteral("label"));
        splitter->addWidget(label);
        label_3 = new QLabel(splitter);
        label_3->setObjectName(QStringLiteral("label_3"));
        splitter->addWidget(label_3);
        label_5 = new QLabel(splitter);
        label_5->setObjectName(QStringLiteral("label_5"));
        splitter->addWidget(label_5);
        label_6 = new QLabel(splitter);
        label_6->setObjectName(QStringLiteral("label_6"));
        splitter->addWidget(label_6);
        label_8 = new QLabel(splitter);
        label_8->setObjectName(QStringLiteral("label_8"));
        splitter->addWidget(label_8);
        button_eliminar = new QPushButton(centralWidget);
        button_eliminar->setObjectName(QStringLiteral("button_eliminar"));
        button_eliminar->setGeometry(QRect(30, 532, 81, 29));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(374, 528, 195, 40));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(layoutWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout->addWidget(pushButton);

        pushButton_2 = new QPushButton(layoutWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);

        TEemail = new QLineEdit(centralWidget);
        TEemail->setObjectName(QStringLiteral("TEemail"));
        TEemail->setGeometry(QRect(100, 190, 451, 31));
        TEemail->setAutoFillBackground(true);
        TEcelular = new QLineEdit(centralWidget);
        TEcelular->setObjectName(QStringLiteral("TEcelular"));
        TEcelular->setGeometry(QRect(100, 230, 180, 31));
        TEcelular->setAutoFillBackground(true);
        frame = new QFrame(centralWidget);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(20, 1, 100, 100));
        QFont font;
        font.setKerning(true);
        frame->setFont(font);
        frame->setFrameShape(QFrame::NoFrame);
        frame->setFrameShadow(QFrame::Raised);
        layoutWidget1 = new QWidget(centralWidget);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(100, 270, 191, 28));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget1);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        primeranio = new QRadioButton(layoutWidget1);
        primeranio->setObjectName(QStringLiteral("primeranio"));
        primeranio->setAutoFillBackground(true);

        horizontalLayout_2->addWidget(primeranio);

        segundoanio = new QRadioButton(layoutWidget1);
        segundoanio->setObjectName(QStringLiteral("segundoanio"));
        segundoanio->setAutoFillBackground(true);

        horizontalLayout_2->addWidget(segundoanio);

        terceranio = new QRadioButton(layoutWidget1);
        terceranio->setObjectName(QStringLiteral("terceranio"));
        terceranio->setAutoFillBackground(true);

        horizontalLayout_2->addWidget(terceranio);

        layoutWidget2 = new QWidget(centralWidget);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(130, 0, 432, 108));
        verticalLayout = new QVBoxLayout(layoutWidget2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_9 = new QLabel(layoutWidget2);
        label_9->setObjectName(QStringLiteral("label_9"));
        QFont font1;
        font1.setPointSize(30);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setUnderline(false);
        font1.setWeight(50);
        font1.setStrikeOut(false);
        label_9->setFont(font1);

        verticalLayout->addWidget(label_9);

        label_10 = new QLabel(layoutWidget2);
        label_10->setObjectName(QStringLiteral("label_10"));
        QFont font2;
        font2.setPointSize(19);
        font2.setBold(false);
        font2.setItalic(true);
        font2.setWeight(50);
        label_10->setFont(font2);

        verticalLayout->addWidget(label_10);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(300, 108, 71, 36));
        TEapellido = new QLineEdit(centralWidget);
        TEapellido->setObjectName(QStringLiteral("TEapellido"));
        TEapellido->setGeometry(QRect(370, 110, 180, 31));
        TEapellido->setAutoFillBackground(true);
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(300, 146, 71, 41));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(300, 230, 71, 34));
        TEfacebook = new QLineEdit(centralWidget);
        TEfacebook->setObjectName(QStringLiteral("TEfacebook"));
        TEfacebook->setGeometry(QRect(370, 230, 180, 31));
        TEfacebook->setAutoFillBackground(true);
        CBcarrera = new QComboBox(centralWidget);
        CBcarrera->setObjectName(QStringLiteral("CBcarrera"));
        CBcarrera->setGeometry(QRect(370, 148, 181, 31));
        MainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        QWidget::setTabOrder(TEnombre, TEapellido);
        QWidget::setTabOrder(TEapellido, TElegajo);
        QWidget::setTabOrder(TElegajo, CBcarrera);
        QWidget::setTabOrder(CBcarrera, TEemail);
        QWidget::setTabOrder(TEemail, TEcelular);
        QWidget::setTabOrder(TEcelular, TEfacebook);
        QWidget::setTabOrder(TEfacebook, primeranio);
        QWidget::setTabOrder(primeranio, segundoanio);
        QWidget::setTabOrder(segundoanio, terceranio);
        QWidget::setTabOrder(terceranio, button_enviar);
        QWidget::setTabOrder(button_enviar, button_limpiar);
        QWidget::setTabOrder(button_limpiar, tableWidget);
        QWidget::setTabOrder(tableWidget, pushButton_2);
        QWidget::setTabOrder(pushButton_2, pushButton);
        QWidget::setTabOrder(pushButton, button_eliminar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionDescargar_Datos->setText(QApplication::translate("MainWindow", "Descargar Datos", 0));
        actionActon->setText(QApplication::translate("MainWindow", "Acton", 0));
        button_enviar->setText(QApplication::translate("MainWindow", "Enviar", 0));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "ID", 0));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "Nombre", 0));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("MainWindow", "Apellido", 0));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("MainWindow", "Synced", 0));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("MainWindow", "Timestamp", 0));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QApplication::translate("MainWindow", "Legajo", 0));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QApplication::translate("MainWindow", "Celular", 0));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QApplication::translate("MainWindow", "Facebook", 0));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget->horizontalHeaderItem(8);
        ___qtablewidgetitem8->setText(QApplication::translate("MainWindow", "Carrera", 0));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidget->horizontalHeaderItem(9);
        ___qtablewidgetitem9->setText(QApplication::translate("MainWindow", "Anio", 0));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidget->horizontalHeaderItem(10);
        ___qtablewidgetitem10->setText(QApplication::translate("MainWindow", "Email", 0));
        button_limpiar->setText(QApplication::translate("MainWindow", "Limpiar", 0));
        label->setText(QApplication::translate("MainWindow", "Nombre", 0));
        label_3->setText(QApplication::translate("MainWindow", "Legajo", 0));
        label_5->setText(QApplication::translate("MainWindow", "Email", 0));
        label_6->setText(QApplication::translate("MainWindow", "Celular", 0));
        label_8->setText(QApplication::translate("MainWindow", "A\303\261o Aveit", 0));
        button_eliminar->setText(QApplication::translate("MainWindow", "Eliminar", 0));
        pushButton->setText(QApplication::translate("MainWindow", "Sync", 0));
        pushButton_2->setText(QApplication::translate("MainWindow", "Salir", 0));
        primeranio->setText(QApplication::translate("MainWindow", "1ero", 0));
        segundoanio->setText(QApplication::translate("MainWindow", "2do", 0));
        terceranio->setText(QApplication::translate("MainWindow", "3ero", 0));
        label_9->setText(QApplication::translate("MainWindow", "Campa\303\261a de Socios", 0));
        label_10->setText(QApplication::translate("MainWindow", "Pre Inscripci\303\263n", 0));
        label_2->setText(QApplication::translate("MainWindow", "Apellido", 0));
        label_4->setText(QApplication::translate("MainWindow", "Carrera", 0));
        label_7->setText(QApplication::translate("MainWindow", "Facebook", 0));
        CBcarrera->clear();
        CBcarrera->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "<Seleccionar>", 0)
         << QApplication::translate("MainWindow", "Civil", 0)
         << QApplication::translate("MainWindow", "El\303\251ctrica", 0)
         << QApplication::translate("MainWindow", "Electr\303\263nica", 0)
         << QApplication::translate("MainWindow", "Industrial", 0)
         << QApplication::translate("MainWindow", "Mec\303\241nica", 0)
         << QApplication::translate("MainWindow", "Metal\303\272rgica", 0)
         << QApplication::translate("MainWindow", "Quimica", 0)
         << QApplication::translate("MainWindow", "Sistemas", 0)
        );
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
