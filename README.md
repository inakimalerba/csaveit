# README #

Formulario simple de carga de socios.

### Que hace? ###

* Recibe datos ingresados por el data entry clerk.
* Guarda datos en SQLite
* Muestra tabla en pantalla
* Sincronizar con DB principal
* Agregar campos
* Validar datos

### Pendiente ###
 
* Some kind of Facebook linking

### Autores ###

* Iñaki Malerba (inakimmalerba@gmail.com)
* Esteban Bosse Hensel (estebanbosse@gmail.com)