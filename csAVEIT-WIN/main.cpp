// Copyright (C) 2015 Iñaki Malerba
// Agrupación Vocacional de Estudiantes e Ingenieros Tecnológicos - AVEIT
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with this.  If not, see <http:www.gnu.org/licenses/>.
//
// Author:    Iñaki Malerba - inakimmalerba@gmail.com
//
// Date: May 2015

#include "mainwindow.h"
#include <QApplication>

#include <QtSql>
#include <QtDebug>
#include <inscripcion.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}

