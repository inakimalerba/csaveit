// Copyright (C) 2015 Iñaki Malerba
// Agrupación Vocacional de Estudiantes e Ingenieros Tecnológicos - AVEIT
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with this.  If not, see <http:www.gnu.org/licenses/>.
//
// Author:    Iñaki Malerba - inakimmalerba@gmail.com
//
// Date: May 2015

#ifndef DBHELPER_H
#define DBHELPER_H

#include <QtSql>
#include <QtDebug>
#include <inscripcion.h>
#include <funciones.h>
#include <QDateTime>
#include <qsqldatabase.h>
#include <stdio.h>
#include <stdlib.h>
//#include <mysql++/mysql++.h>


class dbHelper
{

    // just going to input the general details and not the port numbers


public:
    dbHelper();
    bool init();
    bool crearRegistro(inscripcion socio);
    bool eliminarRegistro(QString id);
    bool connectMysql();
};

#endif // DBHELPER_H
