// Copyright (C) 2015 Iñaki Malerba
// Agrupación Vocacional de Estudiantes e Ingenieros Tecnológicos - AVEIT
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with this.  If not, see <http:www.gnu.org/licenses/>.
//
// Author:    Iñaki Malerba - inakimmalerba@gmail.com
//
// Date: May 2015

#include "funciones.h"
#include <time.h>
using namespace std;

funciones::funciones()
{
}

/**
 * @brief funciones::timestamp
 * @return
 *
 * Devuelve un timestamp formateado como usa MYSQL
 */
QString funciones::timestamp(){

    struct tm *Tm;
    time_t ltime; /* calendar time */
    ltime=time(NULL); /* get current cal time */
    Tm = localtime(&ltime);

    std::stringstream timestamp, mon, day, hour, min, sec;
    mon << setfill('0') << setw(2) << Tm->tm_mon+1;
    day << setfill('0') << setw(2) << Tm->tm_mday;
    hour << setfill('0') << setw(2) << Tm->tm_hour;
    min << setfill('0') << setw(2) << Tm->tm_min;
    sec << setfill('0') << setw(2) << Tm->tm_sec;
    timestamp << Tm->tm_year+1900 << "-" << mon.str() << "-" << day.str() << " " << hour.str() << ":" << min.str() << ":" << sec.str();

    return QString::fromStdString(timestamp.str());

}
