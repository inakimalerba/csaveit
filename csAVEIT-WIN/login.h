#ifndef LOGIN_H
#define LOGIN_H

#include <QDialog>
#include <qprocess.h>
#include <QDebug>
//Esta ventana fue creada con la unica intencion de que los usuarios no puedan intentar syncronizar
//con el servidor offline y se cuelgue el programa. La contraseña es suministrado cuando el servidor esta online.
namespace Ui {
class Login;
}

class Login : public QDialog
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = 0);
    ~Login();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::Login *ui;
};

#endif // LOGIN_H
