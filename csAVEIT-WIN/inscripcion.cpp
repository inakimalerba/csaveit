// Copyright (C) 2015 Iñaki Malerba
// Agrupación Vocacional de Estudiantes e Ingenieros Tecnológicos - AVEIT
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with this.  If not, see <http:www.gnu.org/licenses/>.
//
// Author:    Iñaki Malerba - inakimmalerba@gmail.com
//
// Date: May 2015

#include "inscripcion.h"
using namespace std;
/**
 * @brief inscripcion::inscripcion
 * @param nombre
 * @param apellido
 * @param email
 * @param legajo
 * @param celular
 * @param facebook
 * @param carrera
 * @param anio
 *
 * Clase Inscripcion. Contiene todos los registros de SQLite y MYSQL
 */

inscripcion::inscripcion(QString nombre, QString apellido, QString email, QString legajo, QString celular, QString facebook, QString carrera, QString anio){
    this->nombre = nombre;
    this->apellido = apellido;
    this->email = email;
    this->legajo = legajo;
    this->celular = celular;
    this->facebook = facebook;
    this->carrera = carrera;
    this->anio = anio;

}

void inscripcion::setNombre (QString nombre) {
    this->nombre = nombre;
}

void inscripcion::setApellido (QString apellido) {
    this->apellido = apellido;
}

void inscripcion::setEmail (QString email) {
    this->email = email;
}
void inscripcion::setID(QString id) {
    this->id = id;
}

QString inscripcion::getNombre(){
    return this->nombre;
}

QString inscripcion::getApellido(){
    return this->apellido;
}

QString inscripcion::getID(){
    return this->id;
}

QString inscripcion::getEmail(){
    return this->email;
}

QString inscripcion::getLegajo(){
    return this->legajo;
}

QString inscripcion::getCelular(){
    return this->celular;
}

QString inscripcion::getFacebook(){
    return this->facebook;
}

QString inscripcion::getCarrera(){
    return this->carrera;
}

QString inscripcion::getAnio(){
    return this->anio;
}

