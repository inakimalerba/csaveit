#-------------------------------------------------
#
# Project created by QtCreator 2015-04-29T23:03:58
#
#-------------------------------------------------

QT       += core gui
QT       += core
QT       += network
QT       += sql
QT       -= gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = csAVEIT
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    inscripcion.cpp \
    dbhelper.cpp \
    funciones.cpp

HEADERS  += mainwindow.h \
    inscripcion.h \
    dbhelper.h \
    funciones.h

FORMS    += mainwindow.ui

LIBS += -L/usr/include/mysql
