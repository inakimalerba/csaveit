// Copyright (C) 2015 Iñaki Malerba
// Agrupación Vocacional de Estudiantes e Ingenieros Tecnológicos - AVEIT
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with this.  If not, see <http:www.gnu.org/licenses/>.
//
// Author:    Iñaki Malerba - inakimmalerba@gmail.com
//
// Date: May 2015

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <inscripcion.h>
#include <dbhelper.h>
#include <funciones.h>
#include <QApplication>
#include <QMessageBox>
#include <QDebug>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    dbHelper helper;
    int rowSelected = NULL;
    int anio = NULL;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_tableWidget_cellDoubleClicked(int row, int column);

    void on_button_limpiar_clicked();

    void on_tableWidget_cellClicked(int row, int column);

    void on_button_enviar_pressed();

    void on_button_eliminar_pressed();

    void on_pushButton_pressed();
    void on_primeranio_pressed();

    void on_segundoanio_pressed();

    void on_terceranio_pressed();

    void on_pushButton_2_pressed();

private:
    Ui::MainWindow *ui;
    void populateTable();
    void insertSocioInTable(inscripcion socio);
    void eliminarFila(int row);
    void limpiar();

};

#endif // MAINWINDOW_H
