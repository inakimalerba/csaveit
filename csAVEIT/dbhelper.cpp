// Copyright (C) 2015 Iñaki Malerba
// Agrupación Vocacional de Estudiantes e Ingenieros Tecnológicos - AVEIT
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with this.  If not, see <http:www.gnu.org/licenses/>.
//
// Author:    Iñaki Malerba - inakimmalerba@gmail.com
//
// Date: May 2015

#include "dbhelper.h"

QSqlDatabase db;


dbHelper::dbHelper()
{
}

/**
 * @brief dbHelper::init
 * @return
 *
 * Crea SQLite si no existe
 */
bool dbHelper::init(){
    qDebug( "Init..." );
    db = QSqlDatabase::addDatabase( "QSQLITE" );
    db.setDatabaseName( "./csDB.db" );

    if( !db.open() ){
        qDebug() << db.lastError();
        qFatal( "Failed to connect." );
        db.close();
        return false;
    }
    qDebug( "Connected!" );

    QSqlQuery qry;
    qry.prepare( "CREATE TABLE IF NOT EXISTS inscripciones (id INTEGER UNIQUE PRIMARY KEY, nombre VARCHAR(30), apellido VARCHAR(30), email VARCHAR(30), timestamp DATETIME, legajo VARCHAR(30), celular VARCHAR(30),facebook VARCHAR(30),carrera VARCHAR(30),anio VARCHAR(30), sync VARCHAR(2) DEFAULT 0 )" );

    if( !qry.exec() ){
        qDebug() << qry.lastError();
        db.close();
        return false;
    }
    else{
        qDebug() << "Table created!";
        qDebug( "Initiated" );
        db.close();
        return true;
    }
}


/**
 * @brief dbHelper::crearRegistro
 * @param socio
 * @return
 *
 * Inserta inscripcion en SQLite
 */
bool dbHelper::crearRegistro(inscripcion socio){
        if( !db.open() ){
            qDebug() << db.lastError();
            qFatal( "Failed to connect." );
        }
        qDebug( "Connected!" );

        QSqlQuery qry;
        funciones fn;

        qry.prepare( "INSERT INTO inscripciones (id, nombre, apellido, email, timestamp, legajo, celular, facebook, carrera, anio) VALUES (null, '"+socio.getNombre()+"', '"+socio.getApellido()+"', '"+socio.getEmail()+"', '"+fn.timestamp()+"', '"+socio.getLegajo()+"', '"+socio.getCelular()+"', '"+socio.getFacebook()+"', '"+socio.getCarrera()+"', '"+socio.getAnio()+"')" );
        if( !qry.exec() ){
            qDebug() << qry.lastError();
            db.close();
            return false;
        }
        else{
            qDebug( "Inserted!" );
            db.close();
            return true;
        }

}

/**
 * @brief dbHelper::eliminarRegistro
 * @param id
 * @return
 *
 * Elimina inscripcion de SQLite
 */
bool dbHelper::eliminarRegistro(QString id){
    if( !db.open() ){
        qDebug() << db.lastError();
        qFatal( "Failed to connect." );
    }

    qDebug( "Connected!" );

    QSqlQuery qry;
    funciones fn;

    qry.prepare("DELETE FROM inscripciones WHERE id = '"+id+"'");
    if( !qry.exec() ){
        qDebug() << qry.lastError();
        db.close();
        return false;
    }
    else{
        qDebug( "Inserted!" );
        db.close();
        return true;
    }

}
