// Copyright (C) 2015 Iñaki Malerba
// Agrupación Vocacional de Estudiantes e Ingenieros Tecnológicos - AVEIT
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with this.  If not, see <http:www.gnu.org/licenses/>.
//
// Author:    Iñaki Malerba - inakimmalerba@gmail.com
//
// Date: May 2015

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    helper.init();
    populateTable();

    ui->frame->setStyleSheet("background-image: url(./aveit.png)");
}

MainWindow::~MainWindow()
{
    delete ui;
}

/* Struct para leer las celdas de la tabla al hacer click*/
struct ItemSelected{
    QTableWidgetItem *id,*no, *ap, *em, *leg, *cel, *faceb, *carr, *anio;
};

/**
 * @brief MainWindow::populateTable
 *
 * Llena la tabla con los datos de sqlite al ejecutar el programa
 */

void MainWindow::populateTable(){

    ui->tableWidget->setRowCount(0);
    /* SELECT datos de la tabla*/
    QSqlTableModel model;
    model.setTable("inscripciones");
    model.setFilter("");
    model.setSort(0, Qt::AscendingOrder);
    model.select();

    /* Oculto columnas que no quiero que se vean pero si me interesa guardar sus datos */
    ui->tableWidget->setColumnHidden(0, true);
    ui->tableWidget->setColumnHidden(5, true);
    ui->tableWidget->setColumnHidden(6, true);
    ui->tableWidget->setColumnHidden(7, true);
    ui->tableWidget->setColumnHidden(8, true);
    ui->tableWidget->setColumnHidden(9, true);
    ui->tableWidget->setColumnHidden(10, true);
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);

    int currentRow;

    /*Recorro los registros llenando la tabla*/
    for (int i = 0; i < model.rowCount(); ++i) {

        currentRow = ui->tableWidget->rowCount();
        ui->tableWidget->setRowCount(currentRow + 1);

        QString id = model.record(i).value("id").toString();
        QString nombre = model.record(i).value("nombre").toString();
        QString apellido = model.record(i).value("apellido").toString();
        QString email = model.record(i).value("email").toString();
        QString timestamp = model.record(i).value("timestamp").toString();
        QString sync = model.record(i).value("sync").toString();
        QString legajo = model.record(i).value("legajo").toString();
        QString celular = model.record(i).value("celular").toString();
        QString facebook = model.record(i).value("facebook").toString();
        QString carrera = model.record(i).value("carrera").toString();
        QString anio = model.record(i).value("anio").toString();

        QString synced;

        if(sync == "1") synced = "Sincronizado";
        else synced = "Pendiente";

        ui->tableWidget->setItem(currentRow, 0, new QTableWidgetItem(id));
        ui->tableWidget->setItem(currentRow, 1, new QTableWidgetItem(nombre));
        ui->tableWidget->setItem(currentRow, 2, new QTableWidgetItem(apellido));
        ui->tableWidget->setItem(currentRow, 4, new QTableWidgetItem(timestamp));
        ui->tableWidget->setItem(currentRow, 3, new QTableWidgetItem(synced));
        ui->tableWidget->setItem(currentRow, 5, new QTableWidgetItem(legajo));
        ui->tableWidget->setItem(currentRow, 6, new QTableWidgetItem(celular));
        ui->tableWidget->setItem(currentRow, 7, new QTableWidgetItem(facebook));
        ui->tableWidget->setItem(currentRow, 8, new QTableWidgetItem(carrera));
        ui->tableWidget->setItem(currentRow, 9, new QTableWidgetItem(anio));
        ui->tableWidget->setItem(currentRow, 10, new QTableWidgetItem(email));
    }
}
/**
 * @brief MainWindow::insertSocioInTable
 * @param socio
 *
 * Funcion que inserta socios en la tabla al apretar el boton de enviar.
 * Aclaración: Solo los inserta en QTableWidget. El insert a SQLite es en dbHelper::crearRegistro(inscripcion socio)
 */
void MainWindow::insertSocioInTable(inscripcion socio){

    funciones fn;

    int currentRow = ui->tableWidget->rowCount();
    ui->tableWidget->setRowCount(currentRow + 1);
    ui->tableWidget->setColumnHidden(0, true);
    ui->tableWidget->setColumnHidden(5, true);
    ui->tableWidget->setColumnHidden(6, true);
    ui->tableWidget->setColumnHidden(7, true);
    ui->tableWidget->setColumnHidden(8, true);
    ui->tableWidget->setColumnHidden(9, true);
    ui->tableWidget->setColumnHidden(10, true);

    QString id = socio.getID();
    QString nombre = socio.getNombre();
    QString apellido = socio.getApellido();
    QString email = socio.getEmail();
    QString timestamp = fn.timestamp();
    QString legajo = socio.getLegajo();
    QString celular = socio.getCelular();
    QString facebook = socio.getFacebook();
    QString carrera = socio.getCarrera();
    QString anio = socio.getAnio();

    QString synced = "Pendiente";

    ui->tableWidget->setItem(currentRow, 0, new QTableWidgetItem(id));
    ui->tableWidget->setItem(currentRow, 1, new QTableWidgetItem(nombre));
    ui->tableWidget->setItem(currentRow, 2, new QTableWidgetItem(apellido));
    ui->tableWidget->setItem(currentRow, 4, new QTableWidgetItem(timestamp));
    ui->tableWidget->setItem(currentRow, 3, new QTableWidgetItem(synced));
    ui->tableWidget->setItem(currentRow, 5, new QTableWidgetItem(legajo));
    ui->tableWidget->setItem(currentRow, 6, new QTableWidgetItem(celular));
    ui->tableWidget->setItem(currentRow, 7, new QTableWidgetItem(facebook));
    ui->tableWidget->setItem(currentRow, 8, new QTableWidgetItem(carrera));
    ui->tableWidget->setItem(currentRow, 9, new QTableWidgetItem(anio));
    ui->tableWidget->setItem(currentRow, 10, new QTableWidgetItem(email));

    ui->tableWidget->scrollToBottom();

}


/**
 * @brief MainWindow::on_tableWidget_cellDoubleClicked
 * @param row
 * @param column
 *
 * Al hacer doble click en una fila ofrece eliminar fila
 */
void MainWindow::on_tableWidget_cellDoubleClicked(int row, int column)
{    
    eliminarFila(row);
}

/**
 * @brief MainWindow::on_button_limpiar_clicked
 *
 * Vacía los campos del formulario al presionar el boton Limpiar
 */
void MainWindow::on_button_limpiar_clicked()
{
    limpiar();
}

/**
 * @brief MainWindow::on_tableWidget_cellClicked
 * @param row
 * @param column
 *
 * Llena los campos del formulario con los datos de la fila seleccionada
 */
void MainWindow::on_tableWidget_cellClicked(int row, int column)
{
    rowSelected = row;

    ItemSelected doubleClicked;
    doubleClicked.no = ui->tableWidget->item(row,1);
    doubleClicked.ap = ui->tableWidget->item(row,2);
    doubleClicked.em = ui->tableWidget->item(row,10);
    doubleClicked.leg = ui->tableWidget->item(row,5);
    doubleClicked.cel = ui->tableWidget->item(row,6);
    doubleClicked.faceb= ui->tableWidget->item(row,7);
    doubleClicked.carr = ui->tableWidget->item(row,8);
    doubleClicked.anio = ui->tableWidget->item(row,9);

    ui->TEnombre->setText(doubleClicked.no->text());
    ui->TEapellido->setText(doubleClicked.ap->text());
    ui->TEemail->setText(doubleClicked.em->text());
    ui->TElegajo->setText(doubleClicked.leg->text());
    ui->TEcelular->setText(doubleClicked.cel->text());
    ui->TEfacebook->setText(doubleClicked.faceb->text());
    //ui->TEcarrera->setText(doubleClicked.carr->text());
    ui->CBcarrera->setCurrentText(doubleClicked.carr->text());

    if(doubleClicked.anio->text() == "1") ui->primeranio->setChecked(true);
    else if(doubleClicked.anio->text() == "2") ui->segundoanio->setChecked(true);
    else if(doubleClicked.anio->text() == "3") ui->terceranio->setChecked(true);

}
/**
 * @brief MainWindow::on_button_enviar_pressed
 *
 * Boton enviar. Envía a SQLite y añade en tabla
 */
void MainWindow::on_button_enviar_pressed()
{
    rowSelected = NULL;

    QString nombre = ui->TEnombre->text();
    QString apellido = ui->TEapellido->text();
    QString email = ui->TEemail->text();
    QString legajo = ui->TElegajo->text();
    QString celular = ui->TEcelular->text();
    QString facebook = ui->TEfacebook->text();
    QString carrera = ui->CBcarrera->currentText();
    QString anio;

    if(ui->primeranio->isChecked()) anio = "1";
    else if(ui->segundoanio->isChecked()) anio = "2";
    else if(ui->terceranio->isChecked()) anio = "3";
    if(nombre.isNull() || apellido.isNull() || email.isNull() || legajo.isNull() || celular.isNull() || facebook.isNull() || carrera.isNull() || anio.isNull()){

        /*Muestra adv rellene todos los campos*/
        QMessageBox msgBox;
        msgBox.setWindowTitle("Faltan Campos.");
        msgBox.setText("Por favor complete todos los campos");
        QAbstractButton *ReturnButton = msgBox.addButton(trUtf8("Volver"), QMessageBox::YesRole);
        msgBox.exec();

    }else{

        /* Pone la primera letra en mayus para que sea mas agradable */
        nombre[0] = nombre.at(0).toTitleCase();
        apellido[0] = apellido.at(0).toTitleCase();
        carrera[0] = carrera.at(0).toTitleCase();

        inscripcion socio(nombre, apellido, email, legajo, celular, facebook, carrera, anio); // Crea clase socios enviando datos por constructor

        helper.crearRegistro(socio); //Inserta en SQLite
        insertSocioInTable(socio); //Inserta en QTableWidget

        limpiar(); //Limpia el formulario

    }
}

/**
 * @brief MainWindow::limpiar
 *
 * Limpia los registros del formulario
 */
void MainWindow::limpiar(){

    ui->TEnombre->setText("");
    ui->TEapellido->setText("");
    ui->TEemail->setText("");
    ui->TElegajo->setText("");
    ui->TEcelular->setText("");
    ui->TEfacebook->setText("");
    ui->CBcarrera->setCurrentIndex(0);
}

/**
 * @brief MainWindow::eliminarFila
 * @param row
 *
 * Borra de SQLite y QWidget la fila row
 */
void MainWindow::eliminarFila(int row){

        /* Recupera info del registro de la fila seleccionada */
        ItemSelected doubleClicked;
        doubleClicked.id = ui->tableWidget->item(row,0);
        doubleClicked.no = ui->tableWidget->item(row,1);
        doubleClicked.ap = ui->tableWidget->item(row,2);

        /*Muestra advertencia de borrado*/
        QMessageBox msgBox;
        msgBox.setWindowTitle("Eliminar registro.");
        msgBox.setInformativeText(doubleClicked.no->text() + " " + doubleClicked.ap->text());
        msgBox.setText("¿Está seguro que desea eliminar el registro seleccionado?");
        QAbstractButton *myYesButton = msgBox.addButton(trUtf8("Si"), QMessageBox::YesRole);
        QAbstractButton *myNoButton = msgBox.addButton(trUtf8("No"), QMessageBox::NoRole);
        msgBox.exec();
        if(msgBox.clickedButton() == myYesButton){
            /* Borra */
            helper.eliminarRegistro(doubleClicked.id->text()); //Borra de SQLite
            ui->tableWidget->removeRow(row); //Borra de QWidget

            if(rowSelected != 0) rowSelected = rowSelected-1;

        }else {
        }
}

/**
 * @brief MainWindow::on_button_eliminar_pressed
 *
 * Ofrece eliminar fila al apretar boton Eliminar
 */
void MainWindow::on_button_eliminar_pressed()
{
    eliminarFila(rowSelected);
}


/**
 * @brief MainWindow::on_pushButton_pressed
 *
 * Ejecuta script Python para SYNC con MYSQL
 */

void MainWindow::on_pushButton_pressed()
{
    std::string filename = "./connect.py";
    std::string command = "python ";
    command += filename;
    system(command.c_str());
    populateTable();
}

void MainWindow::on_primeranio_pressed()
{
    anio = 1;
}

void MainWindow::on_segundoanio_pressed()
{
    anio = 2;
}

void MainWindow::on_terceranio_pressed()
{
    anio = 3;
}

void MainWindow::on_pushButton_2_pressed()
{
    /*Muestra advertencia de salir*/
    QMessageBox msgBox;
    msgBox.setWindowTitle("Salir.");
    msgBox.setText("¿Está seguro que desea salir?");
    QAbstractButton *myYesButton = msgBox.addButton(trUtf8("Si"), QMessageBox::YesRole);
    QAbstractButton *myNoButton = msgBox.addButton(trUtf8("No"), QMessageBox::NoRole);
    msgBox.exec();
    if(msgBox.clickedButton() == myYesButton){
        /*Sale*/
        close();
    }else {
    }
}
