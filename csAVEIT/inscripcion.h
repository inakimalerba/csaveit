// Copyright (C) 2015 Iñaki Malerba
// Agrupación Vocacional de Estudiantes e Ingenieros Tecnológicos - AVEIT
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with this.  If not, see <http:www.gnu.org/licenses/>.
//
// Author:    Iñaki Malerba - inakimmalerba@gmail.com
//
// Date: May 2015

#ifndef INSCRIPCION_H
#define INSCRIPCION_H

#include <iostream>
#include <QString>

class inscripcion
{
    QString nombre, apellido, email, timestamp, id, legajo, celular, facebook, carrera, anio;
  public:
    inscripcion(QString nombre, QString apellido, QString email, QString legajo, QString celular, QString facebook, QString carrera, QString anio);
    void setNombre (QString nombre);
    void setApellido (QString apellido);
    void setEmail (QString email);
    void setID (QString id);
    QString getNombre();
    QString getApellido();
    QString getEmail();
    QString getID();
    QString getLegajo();
    QString getCelular();
    QString getFacebook();
    QString getCarrera();
    QString getAnio();

};

#endif // INSCRIPCION_H
