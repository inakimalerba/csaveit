#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2015 Inaki Malerba
# Agrupacion Vocacional de Estudiantes e Ingenieros Tecnologicos - AVEIT
#
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# You should have received a copy of the GNU General Public License
# along with this.  If not, see <http:www.gnu.org/licenses/>.
#
# Author:    Inaki Malerba - inakimmalerba@gmail.com
#
# Date: May 2015

import MySQLdb
import sqlite3 as lite
import sys

con = None

db = MySQLdb.connect(host="cxinaki1.no-ip.org",
user="aveit",
passwd="YPB8ThDdeNSUQJUf",
db="aveit")

curMySQL = db.cursor() 

print '----------------'
print 'Iniciando sincronizacion'

try:
    con = lite.connect('csDB.db')
    cur = con.cursor()

    #Recupera de SQLite todos los que no estan sincronizados
    cur.execute('SELECT * FROM inscripciones WHERE sync = 0')

    rows = cur.fetchall()

    print '----------------'
    print str(len(rows))+ ' elementos a sincronizar'

    #Los recorre y los inserta en MYSQL
    for row in rows:
       
        #Encode por enies y acentos
        nombre = unicode(row[1].encode('utf-8'), 'utf-8')
        apellido = unicode(row[2].encode('utf-8'), 'utf-8')
        email = unicode(row[3].encode('utf-8'), 'utf-8')
        legajo= unicode(row[5].encode('utf-8'), 'utf-8')
        celular = unicode(row[6].encode('utf-8'), 'utf-8')
        facebook = unicode(row[7].encode('utf-8'), 'utf-8')
        carrera = unicode(row[8].encode('utf-8'), 'utf-8')


#        print 'Sincronizando -> '+nombre+' '+ apellido

        #Inserta en MYSQL remoto
        curMySQL.execute("INSERT INTO cs(nombre, apellido, email, timestamp, legajo, celular, facebook, carrera, anio) VALUES "+
        "('"+nombre+"','"+apellido+"','"+email+"','"+row[4]+"','"+legajo+"','"+celular+"','"+facebook+"','"+carrera+"','"+row[9]+"')")
        
        #Marca como sincronizado en SQLITE
        cur.execute("UPDATE inscripciones SET sync = 1 WHERE id = "+str(row[0]));
        con.commit()
        print 'OK'

except lite.Error, e:
    print "Error %s"%e.args[0]
    sys.exit(1)

finally:
    print '----------------'
    print 'Sincronizacion exitosa'
    if con:
        con.close()
        db.commit()
